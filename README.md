# Задание
* https://docs.google.com/document/d/1g_aH0YG1Rk7MJeSXFJXZIobc0z9QuqZdeRv4jdEmJJg/edit

# Установка
* `composer install`
* настроить файл `.env`
* `./artisan migrate --seed`
* в my.cnf в секции [mysqld] создать\изменить параметр `innodb_ft_min_token_size` на значение `1`
* перезапустить СУБД
* `cd public`
* `php -S 127.0.0.1:8081`

# Запросы HTTP GET
* /good/id/{id} - Выдача товара по ID
* /good/title/{title} - Выдача товаров по вхождению подстроки в названии
* /good/producer/{producer} - Выдача товаров по производителю/производителям
* /good/cat_id/{cat_id}/{deep?} - Выдача товаров по разделу (только раздел). Аргумент "deep" - поиск по подкатегориям

# Автор
Шатров Алексей Сергеевич <mail@ashatrov.ru>
