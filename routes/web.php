<?php

use App\Good;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
* Выдача товара по ID
*/
Route::get( '/good/id/{id}' , function ( $id ) {
    return Good::getById( $id ) ;
});

/**
* Выдача товаров по вхождению подстроки в названии
*/
Route::get( '/good/title/{title}' , function ( $title ) {
    return Good::getByTitle( $title ) ;
});

/**
* Выдача товаров по производителю/производителям
*/
Route::get( '/good/producer/{producer}' , function ( $producer ) {
    return Good::getByProducer( $producer ) ;
});

/**
* Выдача товаров по разделу (только раздел)
* Выдача товаров по разделу и вложенным разделам
*/
Route::get( '/good/cat_id/{cat_id}/{deep?}' , function ( $cat_id , $deep = false ) {
    return Good::getByCatId( $cat_id , $deep ) ;
});