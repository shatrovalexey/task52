<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoodTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('good', function (Blueprint $table) {
		$table->increments('id');
		$table->integer('cat_id')->unsigned();
		$table->string('title');
		$table->boolean('availability')->default(false);
		$table->string('producer');
		$table->float('price', 8, 2);

		$table->index('producer');
		$table->index('title');

		$table->foreign('cat_id')->references('id')
			->on('cat')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('good');
    }
}
