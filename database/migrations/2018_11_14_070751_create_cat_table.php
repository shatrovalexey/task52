<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat', function (Blueprint $table) {
		$table->increments('id');
		$table->integer('parent_id')->unsigned( );
		$table->string('title');
		$table->text('parents');

		$table->index('parent_id');
        });

	DB::statement( '
ALTER TABLE `cat`
ADD FULLTEXT `fts_cat_parents`( `parents` )
	' ) ;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat');
    }
}
