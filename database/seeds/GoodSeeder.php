<?php

use Illuminate\Database\Seeder;
use App\Cat ;
use App\Good ;

class GoodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	for ( $i = 0 ; $i < 100 ; $i ++ ) {
		App\Good::store(
			Cat::getRandomId( ) ,
			str_random( 10 ) ,
			rand( 0 , 1 ) ,
			rand( 1 , 1e6 ) ,
			str_random( 10 )
		) ;
	}
    }
}
