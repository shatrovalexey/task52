<?php

use Illuminate\Database\Seeder;
use App\Cat;

class CatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	for ( $i = 0 ; $i < 50 ; $i ++ ) {
		self::seed( ! $i ) ;
	}
    }

	private static function seed( $root = false ) {
		$parent_id = App\Cat::getRandomId( ) ;
		$title = str_random( 10 ) ;

		App\Cat::store( $title , $parent_id , $root ) ;
	}
}
