<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cat extends Model
{
	protected $table = 'cat' ;
	public $timestamps = false ;

	/**
	* Создание записи
	* @param string $title - название категории
	* @param int $parent_id - родительская категория
	*/
	public static function store( $title , $parent_id , $root = false ) {
		$self = new self( ) ;
		$self->title = $title ;

		$self->parents = $parent_id ;

		if ( $cat = self::find( $parent_id ) ) {
			$self->parents .= ',' . $cat->parents ;
		}
		if ( $root ) {
			$self->id = 0 ;
		}

		$self->parent_id = $parent_id ;
		$self->save( ) ;
	}

	/**
	* Получить произвольный существующий ID
	* @return integer
	*/
	public static function getRandomId( ) {
		if ( $cat = self::inRandomOrder( )->first( ) ) {
			return $cat->id ;
		}

		return 0 ;
	}
}
