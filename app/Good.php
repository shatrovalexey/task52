<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Good extends Model
{
	/**
	* @var string $table - имя таблицы
	* @var boolean $timestamps - использовать ли контроль времени
	*/
	protected $table = 'good' ;
	public $timestamps = false ;

	/**
	* Поиск по идентификатору
	* @param integer $id - идентификатор товара
	* @return \Illuminate\Database\Eloquent\Builder
	*/
	public static function getById( $id ) {
		return self::findOrFail( $id ) ;
	}

	/**
	* Поиск по названию
	* @param string $title - название товара
	* @return \Illuminate\Database\Eloquent\Builder
	*/
	public static function getByTitle( $title ) {
		return self::where( 'title' , 'LIKE' , '%' . $title . '%' )->all( ) ;
	}

	/**
	* Поиск по производителю
	* @param string $producer - производитель товара
	* @return \Illuminate\Database\Eloquent\Builder
	*/
	public static function getByProducer( $producer ) {
		return self::where( 'producer' , 'LIKE' , '%' . $producer . '%' )->all( ) ;
	}

	/**
	* Поиск по идентификатору каталога
	* @param integer $cat_id - идентификатор каталога
	* @param boolean $deep = false - нужно ли искать по дочерним каталогам
	* @return \Illuminate\Database\Eloquent\Builder
	*/
	public static function getByCatId( $cat_id , $deep = false ) {
		if ( empty( $deep ) ) {
			return self::where( 'cat_id' , $cat_id ) ;
		}

		self::join( 'cat' , 'good.id' , '=' , 'cat.id' )
			->whereRaw( 'MATCH( `parents` ) AGAINST( ? IN BOOLEAN MODE )' , [ $cat_id ] )
			->orWhere( 'good.cat_id' , '=' , $cat_id ) ;

		return $query->all( ) ;
	}

	/**
	* Сохранение в БД
	* @param integer $cat_id - идентификатор каталога
	* @param string $title - название
	* @param boolean $availability - доступность
	* @param float $price - стоимость
	* @param string $producer - производитель
	*/
	public static function store( $cat_id , $title , $availability , $price , $producer ) {
		$self = new self( ) ;
		$self->cat_id = cat_id ;
		$self->title = $title ;
		$self->availability = $availability ;
		$self->price = $price ;
		$self->producer = $producer ;
		$self->save( ) ;
	}
}
